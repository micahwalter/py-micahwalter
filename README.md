---
title: Py Micah Walter
---

# Py Micah Walter

This is my website, re-imagined in Python. It's gonna do a lot of things, but for now... [homepage!](https://py.micahwalter.com)

I've been thinking about redoing my personal website for a while now. The current site at [https://micahwalter.com](https://micahwalter.com) is fine, but I haven't taken the time to really do much with it, and also, it's not mine! It's a Wordpress website I set up a long time ago with a theme I bought but never really utilized or finished setting up. 

So, I'm going to build a new website from scratch. A long while back I tried to do this and sort of ran out of steam after a cuople of weeks. I didn't really know what I was doing or why, and eventually I just got busy doing other things, so I went back to Wordpress. But this time will be different! 

To start with, I am focusing on building the backend first. It's gonna be a pretty bespokey website. I'm not gonna use CSS or Javascript or REACT! or whatever the new hot thing is. I'm just gonna make a simple website using Python and HTML. I'm sure eventually I'll want to do something fancy and will need some JS or CSS, but until then it's going to be more about finding the data and presenting it on a webpage without much cruft.

## Python

I really enjoy Python. It's the language I always turn to when I need to prototype an idea or figure out a problem, or just do a little math. Python is pretty easy, and to be honest, I find it to be fun. It's very straight forward until it's not, and so I'll try and keep things as simple as possible for the short term without overthinking it. To get myself started I am going to use [Flask](http://flask.pocoo.org/), since Flask is pretty simple itself and means I can make webpages, which is the point, right? Flask comes packaged with [Jinja](http://jinja.pocoo.org/) as a templating engine. Jinja is also super simple and does what I need for the moment.

## Local Development

For local development I am working on my MacBook Pro, writing the code in [VS Code](https://code.visualstudio.com/), and running it via [Pipenv](https://pipenv.readthedocs.io/en/latest/). I've found Pipenv to be the simplest, dumbest way to manage a Python project, so I am gonna use it for this too. It handles all the package management as well as the Python & PIP versioning.

## Hosting

One little requirement I have for this project is that it all runs nicely on [Heroku](https://www.heroku.com). Heroku has been around long enough that I find it simple and flexible enough for me to manage, while at the same time offering a good deal of future options and scaleability that I might need sometime down the road. So, I just need to make sure all my work will run smoothly on Heroku. This means I won't be writing data to disk as part of the application flow, which is probably a good thing anyway.

## GitLab & Documentation

I'm going to be documenting all my work in this [GitLab repository](https://gitlab.com/micahwalter/py-micahwalter). I don't have much experience with GitLab since I've been a GitHub use for years now, but it seems like a really nice platofrm and it's free, so why not?

I'm ging to try and write up little posts about my progress as I go. I'm not sure where to put those yet, but eventually they will become part of the website. Wherever they wind up, I'll be sure to list them here for your easy reference!

Additionally, I'm going to try and make a [practice of screenshotting](https://www.aaronland.info/weblog/2019/01/30/something/) my work as I go, and capturing my notes, sketches, and any design work I wind up developing as things progress.

Here's the first screenshot!

![screenshot #1](https://res.cloudinary.com/micahwalter/image/upload/w_1200/screenshots/Screenshot-2019-03-13-22.48.10.png)

## Ideas

I have a lot of ideas around what I'd like my personal website to be. Mainly though, it's not just a blog or bunch of content about myself, but rather, an archive of things. I'd like to work out ways to collect a lot of the information, content, blog posts, images, tweets, etc. that I've created over the years and put them all in one place with a way to search those things. So, the site is going to start with the data, it's going to start with the low hanging fruit data that I can deal with quickly, and then we will see where things go.

This thing is going to be messy, full of typos, and mistakes. I'm  going to twist and turn and make decisions based on seemingly bizzare ideas, but you'll see, it will be fun!

But first, [homepage](https://py.micahwalter.com)! The site will be hosted here for your viewing pleasure until it is ready for the prime-time domain at [https://micahwalter.com](https://micahwalter.com).

## Feedback

I'd really appreciate any feedback or help along the way. If you want to offer your thoughts, make a correction, or supply some help, you can do all this via [GitLab Issues](https://gitlab.com/micahwalter/py-micahwalter/issues).

-micah