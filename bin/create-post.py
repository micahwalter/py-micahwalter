# a script to create a new post or page from the command line.

import sys
import os
import datetime

import cloudinary
from cloudinary.uploader import upload
from cloudinary.utils import cloudinary_url

import pprint
import requests

def get_brooklyn_int():
    
    payload = {'method': 'brooklyn.integers.create'}
    r = requests.post("https://api.brooklynintegers.com/rest", data=payload)
    
    j = r.json()
    return j['integers']

def create_update():

	now = datetime.datetime.now()
	date = now.strftime("%Y-%m-%d-%H.%M.%S")

	integers = get_brooklyn_int()
	post_id = integers[0]['integer']

	filename = "updates/" + str(post_id) + ".md"
	file = open(filename,'w') 

	file.write('---\n') 
	file.write('title: \n')
	file.write('id: ' + str(post_id) + '\n')
	file.write('created: ' + now.strftime("%Y-%m-%dT%H:%M:%S") +'\n') 
	file.write('---') 

	file.close() 

def create_photo(image_filename):

	integers = get_brooklyn_int()
	post_id = integers[0]['integer']

	filename = "photos/" + str(post_id) + ".md"
    
	response = upload(image_filename, public_id=post_id, folder="photos")

	## TODO Get w_1200 URL
	
	file = open(filename,'w')

	file.write('---\n') 
	file.write('title: \n')
	file.write('id: ' + str(post_id) + '\n')
	file.write('created: ' + response['created_at'] +'\n')
	file.write('url: ' + response['secure_url'] +'\n')
	file.write('---') 

	file.close() 

def create_video(video_filename):

	integers = get_brooklyn_int()
	post_id = integers[0]['integer']

	filename = "videos/" + str(post_id) + ".md"
    
	response = cloudinary.uploader.upload_large(video_filename, public_id=post_id, folder="videos", resource_type="video")
	
	file = open(filename,'w')

	file.write('---\n') 
	file.write('title: \n')
	file.write('id: ' + str(post_id) + '\n')
	file.write('created: ' + response['created_at'] +'\n')
	file.write('url: ' + response['secure_url'] +'\n')
	file.write('---') 

	file.close() 

def main(argv=None):
    
	if argv[1] == "update":
		print("creating an update post")
		create_update()

	if argv[1] == "photo":
		print("creating an photo post")
		# TODO if its a folder, loop through the contents
		image_path = argv[2]

		if os.path.isdir(image_path):
			files = os.listdir(image_path)
			for f in files:
				create_photo(image_path + f)
		else:
			create_photo(image_path)

	if argv[1] == "video":
		print("creating an video post")
		video_filename = argv[2]

		create_video(video_filename)

if __name__ == "__main__":
    main(sys.argv)