from flask import Flask, abort, redirect, request, make_response
from flask import render_template
from flask_talisman import Talisman

from twilio.twiml.messaging_response import Body, Media, Message, MessagingResponse

from elasticsearch import Elasticsearch

import os
import pprint
import random


if 'BONSAI_URL' in os.environ:
    es = Elasticsearch(os.environ['BONSAI_URL'])
else:
    es = Elasticsearch()

import markdown
import markdown.extensions.fenced_code
import frontmatter

app = Flask(__name__)

csp = {
    'default-src': '*',
    'img-src': '*',
    'script-src': '*',
    'style-src': '*',
}

Talisman(app, content_security_policy=csp)

@app.route('/')
def home():

    html, page = open_markdown_file('README.md')

    return render_template('page.html', html=html, page=page)

@app.route('/search', methods=['GET'])
def search():

    if 'q' in request.args:
        q = request.args.get('q')

        body = {
            "query": {
                "match_all": {}
            }
        }

        rsp = es.search(index="micahwalter", q=q, body=body)
        results = rsp['hits']['hits']

        return render_template('search.html', title="Search", results=results)
    else:
        results = []
        return render_template('search.html', title="Search", results=results)


@app.route('/updates/')
def list_updates():

    body = {
        "sort" : [
            { "created" : {"order" : "desc"}},
        ],
        "query": {
            "match": {
                "type" : "updates"
            }
        }
    }

    rsp = es.search(index="micahwalter", body=body)
    updates = rsp['hits']['hits']

    return render_template('updates.html', title="Updates", updates=updates)

@app.route('/updates/<int:update_id>', methods=['GET'])
def show_update(update_id):

    # check for force updates flag
    if request.args.get('update') == "1":
        rsp = index_doc(update_id, "updates")

    # try and load from ES
    try:
        rsp = es.get(index="micahwalter", doc_type="_doc", id=update_id)
    except Exception as e:
        # if its not in ES try and load from disk
        rsp = index_doc(update_id, "updates")
            
    rsp = es.get(index="micahwalter", doc_type="_doc", id=update_id)

    post = rsp['_source']
    return render_template('update.html', post=post)

@app.route('/photos/')
def list_photos():

    page = 1
    page = int(page if not request.args.get('page') else request.args.get('page'))
    
    size = 5
    offset = (page * size) - size

    body = {
        "from" : offset, "size" : size,
        "sort" : [
            { "created" : {"order" : "desc"}},
        ],
        "query": {
            "match": {
                "type" : "photos"
            }
        }
    }

    rsp = es.search(index="micahwalter", body=body)
    photos = rsp['hits']['hits']
    total = rsp['hits']['total']

    total_pages = int(total / size) + (total % size > 0)

    pagination = {
        "page": page,
        "total": total,
        "total_pages": total_pages
    }

    return render_template('photos.html', title="Photos", photos=photos, pagination=pagination)

@app.route('/photos/<int:photo_id>', methods=['GET'])
def show_photo(photo_id):

    # check for force updates flag
    if request.args.get('update') == "1":
        rsp = index_doc(photo_id, "photos")

    # try and load from ES
    try:
        rsp = es.get(index="micahwalter", doc_type="_doc", id=photo_id)
    except Exception as e:
        # if its not in ES try and load from disk
        rsp = index_doc(photo_id, "photos")
            
    rsp = es.get(index="micahwalter", doc_type="_doc", id=photo_id)

    post = rsp['_source']
    return render_template('photo.html', post=post)


@app.route('/videos/<int:video_id>', methods=['GET'])
def show_video(video_id):

    # check for force updates flag
    if request.args.get('update') == "1":
        rsp = index_doc(video_id, "videos")

    # try and load from ES
    try:
        rsp = es.get(index="micahwalter", doc_type="_doc", id=video_id)
    except Exception as e:
        # if its not in ES try and load from disk
        rsp = index_doc(video_id, "videos")
            
    rsp = es.get(index="micahwalter", doc_type="_doc", id=video_id)

    post = rsp['_source']
    return render_template('video.html', post=post)

@app.route('/videos/')
def list_videos():

    body = {
        "sort" : [
            { "created" : {"order" : "desc"}},
        ],
        "query": {
            "match": {
                "type" : "videos"
            }
        }
    }

    rsp = es.search(index="micahwalter", body=body)
    videos = rsp['hits']['hits']

    return render_template('videos.html', title="Videos", videos=videos)

@app.route('/mode')
def set_mode():

    mode = request.cookies.get('mode')

    if (mode == None) or (mode == "light"):
        mode = "dark"
    else:
        mode = "light"

    html, page = open_markdown_file('README.md')

    if mode == "dark":
        resp = make_response(render_template('dark.html'))
    elif mode == "light":
        resp = make_response(render_template('light.html'))

    resp.set_cookie('mode', mode)

    return resp

@app.route('/incoming-sms', methods=['GET', 'POST'])
def incoming_sms():

    resp = MessagingResponse() 
    from_number = request.values.get('From', None)
    from_body = request.values.get('Body', None)
    number = from_number
    body_strip = from_body.lower()
    clean_number = number.strip("+")

    body = {
        "from" : 0, "size" : 100,
        "query": {
            "bool": {
            "must": [
                {
                "query_string": {
                    "query": body_strip,
                    "analyze_wildcard": True,
                    "default_field": "*"
                }
                },
                {
                "match_phrase": {
                    "type": {
                    "query": "photos"
                    }
                }
                }
            ],
            "filter": [],
            "should": [],
            "must_not": []
            }
        }
    }

    rsp = es.search(index="micahwalter", body=body)
    noah = rsp['hits']['hits']

    if len(noah) > 0:
        random.shuffle(noah)
        url = "https://res.cloudinary.com/micahwalter/image/upload/" + "w_300/photos/" + str(noah[0]["_source"]["id"]) + ".jpg"

        message = Message()
        message.body(str(noah[0]["_source"]["title"]))
        message.media(url)
        resp.append(message)
    else:
        resp.message(u"\u2665" + " I'm sorry, but I didn't find anything that matched your query!! Please try again. "  + u"\u2665")
    
    return str(resp)

@app.errorhandler(404)
def page_not_found(error):
	return "404 Not Found!"

def open_markdown_file(filename):

    try:
        input_file = open(filename, mode="r", encoding="utf-8")
    except (OSError, IOError) as e:
        abort(404)
    
    post = frontmatter.load(input_file)
    
    text = post.content
    html = markdown.markdown(text, extensions=['fenced_code'])

    return html, post

def index_doc(doc_id, doc_type):

    filename = doc_type + '/' + str(doc_id) + '.md'
    html, post = open_markdown_file(filename)

    doc = {}
    for key, value in post.metadata.items():
        doc[key] = value

    doc['html'] = html
    doc['type'] = doc_type

    rsp = es.index(index="micahwalter", doc_type="_doc", id=post['id'], body=doc)
    es.indices.refresh(index="micahwalter")

    return rsp