---
title: Playing with Markdown Files
id: 1
slug: playing-with-markdown-files
created: 2019-03-15T10:00:00
---

# Playing with Markdown Files

This is the first update! In fact, you can tell because its URL is `updates/1`

A bunch of stuff happened between yesterday and today. I decided to start by tinkering with markdown files, since that's the way I prefer to write these posts and lots of other things. Fortunately there's a lot of Markdown parsers out there for Python. I chose to keep things simple and use this one called simply [markdown](https://python-markdown.github.io/).

![The README on the homepage](https://res.cloudinary.com/micahwalter/image/upload/w_1200/screenshots/Screenshot-2019-03-14-19.40.41.png)

To get things started I went through the paces of loading in the `README.md` file, and converting it to HTML and then displaying it as the site's homepage. To do this, I wrote a little function that you can see below.

```python
def open_markdown_file(filename):

    try:
        input_file = open(filename, mode="r", encoding="utf-8")
    except (OSError, IOError) as e:
        abort(404)
    
    title = input_file.readline()
    title = title[2:]
    
    text = input_file.read()
    html = markdown.markdown(text)

    return html, title.rstrip()

```
[1835646](https://gitlab.com/micahwalter/py-micahwalter/snippets/1835646)


As a side note, I'll try and paste little snippits of code like this in the project's [snippets](https://gitlab.com/micahwalter/py-micahwalter/snippets) folder, so you can look at them accumulate over time.

The function works like you might think, and after a few rounds of experimenting with it, I got it to do what I want.

## Images

Immediately I noticed that the screenshot image I had in my Markdown file wasn't rendering. This was no big shock since I knew that Flask requires you to set up a dedicated folder to [serve static files](http://flask.pocoo.org/docs/1.0/tutorial/static/). I thought about doing that for a moment, but then I realized that wouldn't be too sustainable, since it's really only meant for local development or small files likes logos and icons. So I decided to start a new [Cloudinary](https://cloudinary.com) account to host images and other kinds of media for the site. Cloudinary is a tool we use at the studio for lots of projects, and I've had a lot of success with it in the past. Plus, they have a free account which should get me pretty far! So, just like that, the website has a Digital Asset Management tool and Content Delivery Network. Isn't the web amazing?

I set it up and noticed right away that my images were not resizing like the rest of the text on my no-css website. I relented, and finally gave in to the idea that I might need to have a little CSS for things like this. SRSLY, it's two lines of CSS.

Now the website is actually pretty mobile friendly!

## Updates

After setting up the CDN and the responsive homepage, I came up with the idea of "updates." Updates will be like posts, or articles, or whatever you want to call them. They are the blog style reports I plan to write about the development of the site, and who knows what else. So to get that going I started by creating a couple new routes. There is one route for an update, and another for a list of all the updates. It's like a blog!

```python
@app.route('/updates/<int:update_id>')
def show_update(update_id):

    filename = 'updates/' + str(update_id) + '.md'
    html, title = open_markdown_file(filename)

    return render_template('update.html', title=title, html=html)
```

[1835649](https://gitlab.com/micahwalter/py-micahwalter/snippets/1835649)

The update route was simple enough. You can see it in the code snippet above. It basically just takes an ID as a parameter from the URL, and tries to load the corresponding Markdown file from disk. I added some error checking to ensure that it will just 404 if you try to load an update that doesn't currently exist.

```python
def list_of_things(folder):

    # simple, dumb way to list all the posts in a folder for now.

    onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]

    things = []

    for item in onlyfiles:
        filename = folder + item
        input_file = open(filename, mode="r", encoding="utf-8")

        title = input_file.readline()
        title = title[2:]
        
        thing = {
            "id" : item[:-3],
            "filename" : item,
            "title" : title.rstrip()
        }
        things.append(thing)

    return things
```

[1835650](https://gitlab.com/micahwalter/py-micahwalter/snippets/1835650)

I also created a route to list all the updates. To do this, I created a function called `list_of_things` that simply reads a directory and returns a list of all the files in there. It makes a few assumptions about these files and tries to return a list of IDs, titles and filenames. Back in the updates route, I use this list to list out all the updates on a webpage, I haven't thought yet about sorting them or paginating them, but I am sure that's next.

![Lists of things](https://res.cloudinary.com/micahwalter/image/upload/w_1200/screenshots/Screenshot-2019-03-14-22.27.23.png)

That's all for now! 

-micah